using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class ReportModel : PageModel
    {
        public goodboy2k1Context _context;
        public List<Report> listreport { get; set; }
        public int notification { get; set; }
        [BindProperty]
        public string UName { get; set; }
        public ReportModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public void OnGet()
        {
            listreport = _context.Reports.Include(x => x.User).OrderBy(x => x.CreateDate).ToList();
            foreach (var notif in listreport)
            {
                if (notif.Status.ToString().ToLower().Equals("false"))
                {
                    notification++;
                }
            }
        }
        public IActionResult OnPost(string submitbutton)
        {

            string name = UName;

            if (submitbutton == "search" && String.IsNullOrEmpty(name))
            {
                listreport = _context.Reports.Include(x => x.User).OrderBy(x => x.CreateDate).ToList();
            }
            else
            {
                listreport = _context.Reports.Include(x => x.User).OrderBy(x => x.CreateDate)
                    .Where(x =>x.User.Name != null && x.User.Name.ToLower().Contains(name)).ToList();
            }
            return Page();
        }
    }
}
