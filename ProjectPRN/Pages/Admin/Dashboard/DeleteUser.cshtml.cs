using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class DeleteUserModel : PageModel
    {
        private goodboy2k1Context _context;
        

        public DeleteUserModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public void OnGet()
        {
          
        }
        public IActionResult OnPost(string submitbutton, [FromRoute] int id)
        {
            if (submitbutton == "delete")
            {
                User user = _context.Users.Where(x => x.Id == id).FirstOrDefault();
                if (user != null)
                {
                    _context.Users.Remove(user);
                    _context.SaveChanges();
                }
                return RedirectToPage("/Admin/Dashboard/table");
            }else if (submitbutton == "cancel")
            {
                return RedirectToPage("/Admin/Dashboard/table");
            }
            return Page();
        }
    }
}
