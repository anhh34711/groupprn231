using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class dashboardModel : PageModel
    {
        public goodboy2k1Context _context;
        public dashboardModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public List<Report> Reports { get; set; }
        public List<Article> Articles { get; set; }
        public int notification { get; set; }
        public int reportfornotif { get; set; }
        public int approvefornotif { get; set; }
        public void OnGet()
        {
            Reports = _context.Reports.OrderBy(x => x.CreateDate).ToList();
            foreach (var reports in Reports)
            {
                if (reports.Status.ToString().ToLower().Equals("false"))
                {
                    notification++;
                    reportfornotif++;
                }
            }
            Articles = _context.Articles.OrderBy(x => x.CreateDate).ToList();
            foreach (var article in Articles)
            {
                if (article.Approve.ToString().ToLower().Equals("new"))
                {
                    notification++;
                    approvefornotif++;
                }
            }
        }
    }
}
