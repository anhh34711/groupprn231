using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class ReportListModel : PageModel
    {
        public goodboy2k1Context _context;
        [BindProperty]
        public List<Report> ReportListByID { get; set; }
        public ReportListModel(goodboy2k1Context context)
        {
            _context = context;
        }

        public void OnGet([FromRoute] int id)
        {
            ReportListByID = _context.Reports.Where(x => x.UserId == id).ToList();
        }
    }
}
