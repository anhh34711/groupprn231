using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class ArticleDetailModel : PageModel
    {
        private goodboy2k1Context _context;
        public string title { get; set; }
        public string content { get; set; }

        public List<Image> Images { get; set; }
        public ArticleDetailModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public void OnGet([FromRoute] int id)
        {
            try
            {
                Article article = _context.Articles.Include(x => x.Images).Where(x => x.Id == id).FirstOrDefault();

                if(article.Images != null)
                {
                    Images = article.Images.ToList();
                }
            
                if (article != null)
                {
                    title = article.Title.ToString();
                    content = article.Content.ToString();
                }
            }catch(Exception ex)
            {

            }
        }
    }
}
