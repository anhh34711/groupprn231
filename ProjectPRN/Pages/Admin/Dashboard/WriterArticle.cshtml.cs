using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class WriterArticleModel : PageModel
    {
        private goodboy2k1Context _context;
        public List<Article> GetArticlesById { get; set; }
        public WriterArticleModel(goodboy2k1Context context)
        {
            _context = context;
        }

        public void OnGet([FromRoute]int id)
        {
            GetArticlesById = _context.Articles.Where(x => x.UserId == id).ToList();
        }
    }
}
