using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class AddWriterModel : PageModel
    {    
        public readonly goodboy2k1Context _context;
        [BindProperty]
        public string? UserName { get; set; }

        [BindProperty]
        public string? FullName { get; set; }
        [BindProperty]
        public string? Password { get; set; }
        public AddWriterModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public void OnGet()
        {
        }
        public IActionResult OnPost(string submitbutton)
        {
            try
            {
                if (submitbutton == "create") {
                    string fullname = FullName;
                    string username = UserName;
                    string password = Password;
                    User writer = new User
                    {
                        Username = username,
                        Name = fullname,
                        Password = HashPassword(password),
                        CreateDate = DateTime.Now,
                        Status = true,
                        RoleId = 2,
                    };
                    if (writer != null)
                    {
                        _context.Users.Add(writer);
                        _context.SaveChanges();
                        TempData["AddWiterSuccess"] = "Add writer successfully";
                    }
                   
                }
            }
            catch (Exception ex)
            {
                TempData["AddWiterFail"] = "Add writer fail";
                return RedirectToPage("/Admin/Dashboard/AddWriter");
            }
            return RedirectToPage("/Admin/Dashboard/user");

        }
        public static string HashPassword(string password)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
