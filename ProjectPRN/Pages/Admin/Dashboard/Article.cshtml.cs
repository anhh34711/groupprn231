using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Data;
using System.Xml.Linq;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class ArticleModel : PageModel
    {
        public goodboy2k1Context _context;
        public List<Article> articles { get; set; }
        [BindProperty]
        public string SelectOption { get; set; }
        [BindProperty]
        public int ArId { get; set; }
        [BindProperty]
        public string WName { get; set; }
        public int notification { get; set; }
        public ArticleModel(goodboy2k1Context context)
        {
            _context = context;
        }

        public void OnGet()
        {

            articles = _context.Articles.Include(x => x.User).OrderBy(x => x.CreateDate).ToList();
            foreach (var ar in articles)
            {
                if (ar.Approve.ToString().ToLower().Equals("new"))
                {
                    notification++;
                }
            }
        }
        public IActionResult OnPost(string submitbutton)
        {
            string name = WName;
            if (submitbutton == "submit")
            {
                try
                {
                    
                    int id = ArId;
                    Article article = _context.Articles.Where(x => x.Id == id).FirstOrDefault();
                    if (article != null)
                    {
                        if (SelectOption.ToString().ToLower().Equals("approve"))
                        {
                            article.Approve = "Approve";
                        }
                        else if (SelectOption.ToString().ToLower().Equals("reject"))
                        {
                            article.Approve = "Reject";
                        }
                        _context.SaveChanges();
                        
                       
                    }
                    
                    TempData["ApproveChangeSuccess"] = "The article's approve change successfully";
                }
                catch (Exception ex)
                {
                    TempData["ApproveChangeFail"] = "The article's approve change fail";
                }
                
            }
            else if (submitbutton == "changestatus")
            {
                try
                {
                    int id = ArId;
                    Article article = _context.Articles.Where(x => x.Id == id).FirstOrDefault();
                    if (article != null)
                    {
                        if (article.Status.ToString().ToLower().Equals("true"))
                        {
                            article.Status = false;
                        }
                        else if (article.Status.ToString().ToLower().Equals("false"))
                        {
                            article.Status = true;
                        }
                        _context.SaveChanges();
                        
                    }
                    
                    TempData["ChangeStatusSuccess"] = "The article's status change successfully";
                }
                catch (Exception ex)
                {
                    TempData["ChangeStatusFail"] = "The article's status change fail";
                } 
               
            }
            else if (submitbutton == "search")
            {
                if (String.IsNullOrEmpty(name)) 
                {
                    articles = _context.Articles.Include(x => x.User).OrderBy(x => x.CreateDate).ToList();
                }
                else
                {
                    articles = _context.Articles.Include(x => x.User).OrderBy(x => x.CreateDate)
                   .Where(x => x.User.Name != null && x.User.Name.ToLower().Contains(name))
                   .ToList();
                }
                return Page();
            }
         

            return RedirectToPage("/Admin/Dashboard/Article");

        }
    }
}
