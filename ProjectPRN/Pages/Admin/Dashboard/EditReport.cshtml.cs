using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class EditReportModel : PageModel
    {
        private goodboy2k1Context _context;
        [BindProperty]
        public int ID { get; set; }
        [BindProperty]
        public string Contents { get; set; }
        [BindProperty]
        public string CreateDate { get; set; }
        [BindProperty]
        public bool Seen { get; set; }
        [BindProperty]
        public bool NotSeen { get; set; }
        public EditReportModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public void OnGet([FromRoute]int id)
        {
            try
            {
                Report report = _context.Reports.Where(x => x.Id == id).FirstOrDefault();
                if (report != null)
                {
                    ID = report.Id;
                    Contents = report.Content.ToString();
                    CreateDate = report.CreateDate.ToString();
                    if (report.Status.ToString().ToLower().Equals("true"))
                    {
                        Seen = true; NotSeen = false;
                    }
                    else
                    {
                        Seen = false; NotSeen = true;
                    }
                    report.Status = true;

                    _context.Update(report);
                    _context.SaveChanges();
                }
            }catch(Exception ex)
            {
                RedirectToPage("/Admin/Report");
            }

        }
        public IActionResult OnPost(string submitbutton)
        {
            //try
            //{
            //    if (submitbutton == "update")
            //    {
            //        int id = ID;
            //        string seen = Seen.ToString();
            //        string notseen = NotSeen.ToString();
            //        Report report = _context.Reports.Where(x => x.Id == id).FirstOrDefault();
            //        if (report != null)
            //        {

            //            if (seen.ToLower().Equals("true"))
            //            {
            //                report.Status = true;
            //            }
            //            else if (notseen.ToLower().Equals("true"))
            //            {
            //                report.Status = false;
            //            }
            //            _context.SaveChanges();
            //            TempData["EditReportSuccess"] = "The information of report has been changed";
            //        }
            //    }
            //}catch(Exception ex)
            //{
            //    TempData["EditReportFail"] = "The information of report has been failed";
            //}
            return RedirectToPage("/Admin/Dashboard/Report");
        }
    }
}
