using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Data;
using System.Xml.Linq;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class tableModel : PageModel
    {
        private readonly goodboy2k1Context _context;
        public List<User> ListUser { get; set; }
        public List<Report> ListReport { get; set; }
        [BindProperty]
        public string UName { get; set; }
        public tableModel(goodboy2k1Context context)
        {
            _context= context;
        }
        public void OnGet()
        {
            ListUser = _context.Users.ToList();
            ListReport = _context.Reports.ToList();
        }
        public IActionResult OnPost(string submitbutton)
        {

            string name = UName;

            if (submitbutton == "search" && String.IsNullOrEmpty(name))
            {
                ListUser = _context.Users
                    .ToList();
            }
            else
            {
                ListUser = _context.Users
                    .Where(x => x.Name != null && x.Name.ToLower().Contains(name))
                    .ToList();
            }
            ListReport = _context.Reports.ToList();
            return Page();
        }
    }
}
