using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class EditUserModel : PageModel
    {
        private goodboy2k1Context _context;
        [BindProperty]
        public string? UserName { get; set; }

        [BindProperty]
        public string? FullName { get; set; }
        [BindProperty]
        public string? Role { get; set; }
        [BindProperty]
        public bool Active { get; set; }

        [BindProperty]
        public bool Inactive { get; set; }
        [BindProperty]
        public int ID { get; set; }
        public EditUserModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public void OnGet([FromRoute]int id)
        {
            try
            {
                User user = _context.Users.Include(x => x.Role).Where(x => x.Id == id).FirstOrDefault();
                if (user != null) 
                {
                    ID = user.Id;
                    UserName = user.Username;
                    FullName = user.Name;
                    Role = user.Role.Name;
                    if (user.Status.ToString().ToLower().Equals("true"))
                    {
                        Active = true; Inactive = false;
                    }
                    else
                    {
                        Active = false; Inactive = true;
                    }
                }
            }catch(Exception ex)
            {
                 RedirectToPage("/Admin/Dashboard");
            }
        }
        public IActionResult OnPost(string submitbutton)
        {
            string fname = FullName;
            string uname = UserName;
            try
            {
                if (submitbutton == "update")
                {
                   
                    string active = Active.ToString();
                    string inactive = Inactive.ToString();
                    int id = ID;

                    User user = _context.Users.Where(x => x.Id == id).FirstOrDefault();
                    if (user != null)
                    {
                        if (active.ToLower().Equals("true"))
                        {
                            user.Status = true;
                        }
                        else if (inactive.ToLower().Equals("true"))
                        {
                            user.Status = false;
                        }
                        _context.SaveChanges();
                        TempData["EditUserSuccess"] = "The information of user has been changed";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["EditUserFail"] = "The information of user has been changer failed ";
            }
            return RedirectToPage("/Admin/Dashboard/table");
        }
    }
}
