using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.Data;

namespace ProjectPRN.Pages.Admin.Dashboard
{
    [Authorize(Roles = "ADMIN")]
    public class userModel : PageModel
    {
        private readonly goodboy2k1Context _context;
        public List<User> ListUser { get; set; }
        public List<Report> ListReport { get; set; }
        public List<Article> ListArticle { get; set; }

        [BindProperty]
        public string WName { get; set; }
        public userModel(goodboy2k1Context context)
        {
            _context = context;
        }
        public void OnGet()
        {
            ListUser = _context.Users.Where(x => x.RoleId == 2).ToList();
            ListReport = _context.Reports.ToList();
            ListArticle = _context.Articles.ToList();
        }

        public IActionResult OnPost(string submitbutton)
        {
            
            string name = WName;
            
            if (submitbutton == "search" && String.IsNullOrEmpty(name))
            {
                ListUser = _context.Users
                    .Where(x => x.RoleId == 2)
                    .ToList();
            }
            else
            {
                ListUser = _context.Users
                    .Where(x => x.RoleId == 2 && x.Name != null && x.Name.ToLower().Contains(name))
                    .ToList();
            }
            ListReport = _context.Reports.ToList();
            ListArticle = _context.Articles.ToList();
            return Page();
        }
    }
}
