﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Security.Claims;
using System.Security.Cryptography;

namespace ProjectPRN.Pages.DetailArticle
{
    public class blog_detail_01Model : PageModel
    {
		private readonly goodboy2k1Context _context;

		public blog_detail_01Model(goodboy2k1Context context)
		{
			_context = context;
		}
		[BindProperty]
		public string action { get; set; }
		public bool error { get; set; } = true;
		public static int Aid { set; get; }
		[BindProperty]
		public string Msg { get; set; }
		[BindProperty]
		public string MsgRP { get; set; }
		public Article currentArticle { get; set; }
		public List<Image> Images { get; set; }

		[BindProperty]
		public List<Category> Categories { get; set; }

		// Danh sách bình luận
		public List<Comment> Comments { get; set; }

		public void OnGet()
		{
			if (HttpContext.User.Identity.IsAuthenticated)
			{
				ViewData["user"] = GetClaimInHttpContext(ClaimTypes.UserData);
				if (GetClaimInHttpContext(ClaimTypes.Role).Trim().ToLower().Equals("admin"))
				{
					ViewData["role"] = "admin";
				}
				else if (GetClaimInHttpContext(ClaimTypes.Role).Trim().ToLower().Equals("writer"))
				{
					ViewData["role"] = "writer";
				}
			}
			try
			{
				Categories = _context.Categories.Where(a => a.Status == true).ToList();
				Aid = int.Parse(Request.Query["id"].ToString());
				//check

				if (_context.Articles.Where(c => c.Id == Aid).FirstOrDefault() == null)
				{
					error = false;

				}

				currentArticle = _context.Articles
					.Include(x => x.Cate).Include(x => x.User)
					.Include(x => x.Images)
					.Where(c => c.Id == Aid)
					.FirstOrDefault();
				if (currentArticle.Images != null)
				{
					Images = currentArticle.Images.ToList();
				}
				Comments = _context.Comments.Include(x => x.User).Where(c => c.ArticleId == Aid && c.Status == true).ToList();
			} catch (Exception ex)
			{
				error = false;
			}
		}

		public IActionResult OnPostAsync()
		{
			if (HttpContext.User.Identity.IsAuthenticated)
			{
				ViewData["user"] = GetClaimInHttpContext(ClaimTypes.UserData);
				if (GetClaimInHttpContext(ClaimTypes.Role).Trim().ToLower().Equals("admin"))
				{
					ViewData["role"] = "admin";
				}
				else if (GetClaimInHttpContext(ClaimTypes.Role).Trim().ToLower().Equals("writer"))
				{
					ViewData["role"] = "writer";
				}
			}
			if (!HttpContext.User.Identity.IsAuthenticated)
			{
				return RedirectToPage("/Account/login");
			}
             Categories = _context.Categories.Where(a => a.Status == true).ToList();
			if (action == "Report")
			{
				try
				{
					Report report = new Report
					{
						ArticleId = Aid,
						Status = true,
						UserId = int.Parse(GetClaimInHttpContext(ClaimTypes.UserData).Trim()),
						Content = MsgRP,
						CreateDate = DateTime.Now,
					};
					_context.Reports.Add(report);
					_context.SaveChanges();
					TempData["error"] = "Report successful";
				}
				catch (Exception ex)
				{
					TempData["error"] = "an error happened " + ex.Message;
					return Redirect("/DetailArticle/blog-detail-01?id=" + Aid);
				}
				
				return Redirect("/DetailArticle/blog-detail-01?id=" + Aid);
			}

			if (action == "Comment") {
				var comment = new Comment
				{
					UserId = int.Parse(GetClaimInHttpContext(ClaimTypes.UserData).Trim())
					,
					Content = Msg,
					CreateDate = DateTime.Now,
					Status = true,
					ArticleId = Aid
				};
				_context.Comments.Add(comment);
				_context.SaveChanges();
				return Redirect("/DetailArticle/blog-detail-01?id=" + Aid);
			}
			return Redirect("/DetailArticle/blog-detail-01?id=" + Aid);
		}
			
		
        
        public string GetClaimInHttpContext(string ClaimType)
		{
			if (!HttpContext.User.Identity.IsAuthenticated)
			{
				RedirectToPage("/Account/Login");
				return string.Empty;
			}
			return HttpContext.User.Claims.Where(c => c.Type.Equals(ClaimType)).FirstOrDefault().Value;
		}
	}
}
