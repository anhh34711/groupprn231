﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace ProjectPRN.Pages.Account
{
    public class LoginModel : PageModel
    {
        private goodboy2k1Context context;

        [BindProperty]
        [Required(ErrorMessage = "vui lòng nhập email")]
        public string Name { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "vui lòng nhập password")]
        public string Pass { get; set; }

        public LoginModel(goodboy2k1Context _context) {
            context = _context;
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost() {
           
			if (ModelState.IsValid == false)
            {
                return Page();
            }

            User user = context.Users.Include(x => x.Role). Where(c => c.Username.Equals(Name.Trim()) && c.Password.Equals(SignupModel.HashPassword(Pass.Trim()))).FirstOrDefault();

            if (user == null)
            {
                return RedirectToPage("/Account/Login");
            }
            else
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Name),
                    new Claim(ClaimTypes.Role, user.Role.Name),
                    new Claim(ClaimTypes.NameIdentifier, user.Username),
                    new Claim(ClaimTypes.DateOfBirth, user.CreateDate.ToString()),
					new Claim(ClaimTypes.UserData, user.Id.ToString())
				};
                var identity = new ClaimsIdentity(claims, "token");
                var principal = new ClaimsPrincipal(identity);
                HttpContext.SignInAsync(principal, new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(3)
                });
                return RedirectToPage("/Homepage/Index");
            }
        }
    }
}
