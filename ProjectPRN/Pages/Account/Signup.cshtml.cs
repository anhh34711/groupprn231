﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace ProjectPRN.Pages.Account
{
    public class SignupModel : PageModel
    {
        private goodboy2k1Context context;

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập tên")]
        public string LName { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập tên")]
        public string FName { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập email")]
        public string Email { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập Password")]
        [MinLength(6, ErrorMessage = "passeword phải lớn hơn 6 kí tự")]
        public string Pass { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập lại password")]       
        public string PassConfirm { get; set; }

        public SignupModel(goodboy2k1Context _context)
        {
            context = _context;
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            if(ModelState.IsValid == false)
            {  
                return Page();
            }
            if (!Pass.Trim().Equals(PassConfirm.Trim()))
            {
                Pass = "Password confirm không khớp";
                return Page();
            }
            if(context.Users.Where(c=>c.Username.Equals(Email.Trim())).FirstOrDefault() != null)
            {
                Email = "Email đã được sử dụng";
                return Page();
            }
            try
            {
                User user = new User
                {
                    Username = Email,
                    Password = HashPassword(Pass.Trim()),
                    Name = LName + " " + FName,
                    CreateDate = DateTime.Now,
                    RoleId = 3,
                    Status = true,
                };
                context.Users.Add(user);
                context.SaveChanges();

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, LName + " " + FName),
                    new Claim("id", user.Id.ToString()),
                    new Claim(ClaimTypes.Role, context.Roles.Where(c=>c.Id == 3).FirstOrDefault().Name),
                    new Claim(ClaimTypes.Email, Email),
                    new Claim("create_date", DateTime.Now.ToString())
                };
                var identity = new ClaimsIdentity(claims, "token");
                var principal = new ClaimsPrincipal(identity);
                HttpContext.SignInAsync(principal, new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(3)
                });
            }
            catch(Exception ex)
            {
                Email = "Xảy ra lỗi không thể tạo tài khoản, hãy thử lại sau ít phút.";
                return Page();
            }
            return RedirectToPage("/Account/Login");
        }

        public static string HashPassword(string password)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

    }
}
