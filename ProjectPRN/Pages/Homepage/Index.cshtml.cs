using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Security.Claims;

namespace ProjectPRN.Pages.Homepage
{
	public class IndexModel : PageModel
	{
		private readonly goodboy2k1Context context;

		[BindProperty]
		public List<Category> categories { set; get; }

		[BindProperty]
		public List<Article> Articles { set; get; }

		public List<Article> ArticleNew { set; get; }
		public IndexModel(goodboy2k1Context _context)
		{
			this.context = _context;
		}


		public async void OnGet()
		{
			if (HttpContext.User.Identity.IsAuthenticated)
			{
				ViewData["user"] = GetClaimInHttpContext(ClaimTypes.UserData);
				if (GetClaimInHttpContext(ClaimTypes.Role).Trim().ToLower().Equals("admin"))
				{
					ViewData["role"] = "admin";
				}else if (GetClaimInHttpContext(ClaimTypes.Role).Trim().ToLower().Equals("writer"))
				{
					ViewData["role"] = "writer";
				}
			}

			string search = Request.Query["search"];
			string title = Request.Query["title"];
			int catId = 0;

			categories = context.Categories.Where(a => a.Status == true).ToList();
			if (title != null && title.Trim() != string.Empty)
			{
				ViewData["cate"] = title.Trim();
				Category c = context.Categories.Where(a => a.Status == true && a.Title.Trim().Equals(title.Trim())).FirstOrDefault();
				if (c != null)
				{
					catId = c.Id;
				}
			}
			
			
			if (search != null && search.Trim() != string.Empty)
			{
				Articles = context.Articles.Include(a => a.Images)
				.Where(a => (catId == 0 ? true : a.CateId == catId) && a.Approve.ToLower().Equals("approve") && a.Status == true && a.Title.Contains(search.Trim()))
				.OrderBy(c => c.CreateDate).ToList();
				
			}
			else
			{
				Articles = context.Articles.Include(a => a.Images)
								.Where(a => catId == 0 ? true : a.CateId == catId && a.Approve.ToLower().Equals("approve") && a.Status == true)
								.OrderBy(c => c.CreateDate).ToList();
			}

			ArticleNew = context.Articles.Include(a => a.Images).OrderBy(c => c.CreateDate).ToList();
			if (ArticleNew != null)
			{
				ArticleNew = ArticleNew.Where(a => a.Status == true && a.Approve.ToLower().Equals("approve") && (DateTime.Now - a.CreateDate).Value.TotalHours <= 24).ToList();
			}

		}

		public void OnPost()
		{

		}

		public string GetClaimInHttpContext(string ClaimType)
		{
				return HttpContext.User.Claims.Where(c => c.Type.Equals(ClaimType)).FirstOrDefault().Value;
		}
	}
}