﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN.Models;
using System.Security.Claims;
using System.Security.Cryptography.Xml;
using System.Security.Principal;

namespace ProjectPRN.Pages.Profile
{
    [Authorize(Roles = "USER")]
    public class UserProfileModel : PageModel
    {
		private goodboy2k1Context _context;
		[BindProperty]
		public string? fullname { get; set; }
		[BindProperty]
		public string? username { get; set; }
        public string? FullName { get; set; }
        public string? Username { get; set; }
        public DateTime CreateDate { get; set; }
        public int UserId { get; set; }
		
		public UserProfileModel(goodboy2k1Context context) 
		{
			_context = context;
		}
        
        public void OnGet()
        {
			if (ModelState.IsValid)
			{
				int uid = 0;
				try
				{
					ClaimsPrincipal user = HttpContext.User;
					IEnumerable<Claim> claims = user.Claims;
					foreach (Claim claim in claims)
					{
						if (claim.Type.Equals(ClaimTypes.UserData))
						{
							uid = Int32.Parse(claim.Value);
						}

					}
					User userlogin = _context.Users.Where(x => x.Id == uid).FirstOrDefault();
					if (userlogin != null)
					{
						FullName = userlogin.Name;
						Username = userlogin.Username;
						CreateDate = (DateTime)userlogin.CreateDate;
						UserId = uid;
					}

				}
				catch (Exception ex)
				{

				}
			}
        }
		public IActionResult OnPost(string submitbutton)
		{
			try
			{
				int uid = 0;
				ClaimsPrincipal user = HttpContext.User;
				IEnumerable<Claim> claims = user.Claims;
				foreach (Claim claim in claims)
				{
					if (claim.Type.Equals(ClaimTypes.UserData))
					{
						uid = Int32.Parse(claim.Value);
					}

				}
				if (submitbutton == "update")
				{
					if (ModelState.IsValid)
					{
						string fullName = fullname;
						string userName = username;
						User userUpdate = _context.Users.Where(x => x.Id == uid).FirstOrDefault();						
						if (userUpdate != null)
						{
							userUpdate.Name = fullName;
							userUpdate.Username = userName;
							_context.SaveChanges();
						}
					}
				}
				
			}
			catch(Exception ex)
			{

			}
			return RedirectToPage("/Profile/UserProfile");
		}
    }
}
