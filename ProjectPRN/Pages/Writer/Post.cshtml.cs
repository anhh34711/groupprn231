﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ProjectPRN.Models;
using System;
using System.Security.Claims;

namespace ProjectPRN.Pages.Writer
{
    [Authorize(Roles = "WRITER")]
    public class PostModel : PageModel
    {
        public Article Article { get; set; }
        [BindProperty]
        public List<IFormFile> Files { get; set; }
        public List<Category> Categories { get; set; }
        public byte[] img { get; set; }
        private readonly ILogger<PostModel> Logger;
        private goodboy2k1Context context;

        [BindProperty]
        public string Content { get; set; }

        [BindProperty]
        public string Title { get; set; }

        [BindProperty]
        public string Description { get; set; }


        public PostModel(goodboy2k1Context _context, ILogger<PostModel> logger)
        {
            context= _context;
            this.Logger = logger;
        }

        public void OnGet()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                ViewData["user"] = GetClaimInHttpContext(ClaimTypes.UserData);
            }
            Categories = context.Categories.ToList();
        }

        public  IActionResult OnPost() {
            Categories = context.Categories.ToList();
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                ViewData["user"] = GetClaimInHttpContext(ClaimTypes.UserData);
            }
            if (!ModelState.IsValid)
            {
                ViewData["error1"] = "các trường không được bỏ trống";
                return Page();
            }
            var c = HttpContext.Request.Form["cate"];
            var files = HttpContext.Request.Form.Files;
            List<byte[]> bytes = new List<byte[]>();
            try
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        using (var stream = new MemoryStream())
                        {
                            file.CopyTo(stream);
                            byte[] imageData = stream.ToArray();
                            bytes.Add(imageData);

                        }
                    }
                }

                Article article = new Article
                {
                    Approve = "NEW",
                    CreateDate = DateTime.Now,
                    Content = Content,
                    Title = Title,
                    Description= Description,
                    Status = true,
                    CateId = int.Parse(c),
                    UserId = int.Parse(GetClaimInHttpContext(ClaimTypes.UserData).Trim())
                };
                
                context.Articles.Add(article);
                context.SaveChanges();

                List<Image> images = new List<Image>();
                foreach (var b in bytes)
                {
                    images.Add(new Image
                    {
                        Image1 = b,
                        ArticleId = article.Id,
                    });
                }

                context.Images.AddRange(images);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.LogInformation("xảy ra lỗi " + ex.Message);
                ViewData["error1"] = "tạo bài báo xảy ra lỗi " + ex.Message;
                return Page();
            }
            ViewData["error"] = "tạo bài báo thành công, chờ phê duyệt";
            return Page();
        }

        public string GetClaimInHttpContext(string ClaimType)
        {
           return HttpContext.User.Claims.Where(c=>c.Type.Equals(ClaimType)).FirstOrDefault().Value;
        }
    }
    
}
