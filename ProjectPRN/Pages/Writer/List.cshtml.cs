using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN.Models;
using System.Security.Claims;

namespace ProjectPRN.Pages.Writer
{
    //[Authorize(Roles = "WRITER")]
    public class ListModel : PageModel
    {
        private readonly ILogger<PostModel> Logger;
        private goodboy2k1Context context;

        public ListModel(goodboy2k1Context _context, ILogger<PostModel> logger)
        {
            context = _context;
            this.Logger = logger;
        }

        [BindProperty]
        public List<Article> Articles { get; set; }

        public void OnGet()
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                ViewData["userid"] = GetClaimInHttpContext(ClaimTypes.UserData);
            }
                Articles = context.Articles.Include(c=>c.Images)
                //.Where(c=>c.UserId.ToString().Equals(GetClaimInHttpContext(ClaimTypes.UserData).Trim()))
                .ToList();
        }

        public void Onpost()
        {
            var id = int.Parse(Request.Form["id"].ToString());
            int g = id;
            context.Images.RemoveRange(context.Images.Where(c=>c.ArticleId == id).ToList());
            context.SaveChanges();
            context.Articles.Remove(context.Articles.Where(c=>c.Id== id).FirstOrDefault());
            context.SaveChanges();
            RedirectToPage("/Writer/List");
        }
        public string GetClaimInHttpContext(string ClaimType)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                RedirectToPage("/Account/Login");
                return string.Empty;
            }
            else
            {
                return HttpContext.User.Claims.Where(c => c.Type.Equals(ClaimType)).FirstOrDefault().Value;
            }
            }
    }
}
