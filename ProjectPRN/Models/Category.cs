﻿using System;
using System.Collections.Generic;

namespace ProjectPRN.Models
{
    public partial class Category
    {
        public Category()
        {
            Articles = new HashSet<Article>();
        }

        public int Id { get; set; }
        public string? Title { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<Article> Articles { get; set; }
    }
}
