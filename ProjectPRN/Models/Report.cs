﻿using System;
using System.Collections.Generic;

namespace ProjectPRN.Models
{
    public partial class Report
    {
        public int Id { get; set; }
        public string? Content { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        
        public int? ArticleId { get; set; }
        public bool Status { get; set; }

        public virtual Article? Article { get; set; }
        public virtual User? User { get; set; }
    }
}
