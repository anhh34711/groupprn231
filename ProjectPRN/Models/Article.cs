﻿using System;
using System.Collections.Generic;

namespace ProjectPRN.Models
{
    public partial class Article
    {
        public Article()
        {
            Comments = new HashSet<Comment>();
            Images = new HashSet<Image>();
            Reports = new HashSet<Report>();
        }

        public int Id { get; set; }
        public string? Title { get; set; }

        public string? Description { get; set; }
        public string? Content { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool Status { get; set; }
        public string? Approve { get; set; }
        public int? CateId { get; set; }

        public virtual Category? Cate { get; set; }
        public virtual User? User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
    }
}
