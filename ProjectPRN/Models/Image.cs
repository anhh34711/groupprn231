﻿using System;
using System.Collections.Generic;

namespace ProjectPRN.Models
{
    public partial class Image
    {
        public int Id { get; set; }
        public byte[]? Image1 { get; set; }
        public int? ArticleId { get; set; }

        public virtual Article? Article { get; set; }
    }
}
