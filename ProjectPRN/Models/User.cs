﻿using System;
using System.Collections.Generic;

namespace ProjectPRN.Models
{
    public partial class User
    {
        public User()
        {
            Articles = new HashSet<Article>();
            Comments = new HashSet<Comment>();
            Reports = new HashSet<Report>();
        }

        public int Id { get; set; }
        public string? Username { get; set; }
        public string? Name { get; set; }
        public string? Password { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Status { get; set; }
        public int RoleId { get; set; }

        public virtual Role? Role { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
    }
}
